class profile_apt() {
  if $::osfamily == "Debian" {
    class { 'apt':
      update => {
        frequency => 'always',
      },
    }
    Class['apt::update'] -> Package <||>
    apt::source { 'bintray':
      location  => 'https://dl.bintray.com/evankrall/deb',
      repos     => 'main',
      key       => {
        id      => '8756C4F765C9AC3CB6B85D62379CE192D401AB61',
        content => file('profile_common/bintray.key'),
      }
    }
  }
}

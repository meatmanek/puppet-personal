class profile_mdns {
  if $::operatingsystem != 'Darwin' {
    include avahi
  }
  if $::operatingsystem == 'Ubuntu' {
    package { 'libnss-mdns':
      ensure => 'latest',
    }
  }
}

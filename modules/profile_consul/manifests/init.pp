class profile_consul(
  $servers = hiera('profile_consul::servers', undef),
  $monitors = hiera('profile_consul::monitors', undef),
  $datacenter = hiera('profile_consul::datacenter', undef),
  $node_name = $::fqdn,
) {
  if ($node_name in $servers) {
    $bootstrap_expect = size($servers)
    $serve = true
  } else {
    $bootstrap_expect = 0
    $serve = false
  }

  file { '/opt':
    ensure => 'directory',
    owner  => 'root',
    group  => 0,
    mode   => '0755',
  } ->
  File ['/opt/consul'] ->
  exec { 'generate consul csr':
    command => shellquote([
      'openssl', "req",
      "-new",
      "-nodes",
      "-newkey",
      "rsa:2048",
      "-keyout", "/opt/consul/consul.key",
      "-out", "/opt/consul/consul.csr",
      "-days", "365",
      "-subj", "/C=US/ST=California/L=San Francisco/O=Evan Krall/CN=${node_name}.consul/",
    ]),
    umask   => '0077',
    user    => 'consul',
    creates => ['/opt/consul/consul.csr', '/opt/consul/consul.key'],
  } ->
  file { '/opt/consul/consul.key':
    ensure => present,
    mode   => '0400',
    owner  => 'consul',
  }

  file { '/opt/consul/ca.crt':
    source => 'puppet:///modules/profile_consul/CA/certs/ca.crt',
    mode => 0444,
  } -> Service['consul']

  $sanitized_node_name = regsubst($node_name, '\.', '_', 'G')
  if hiera("consul::cert_signed::${sanitized_node_name}", false) {
    file { '/opt/consul/consul.pem':
      source => "puppet:///modules/profile_consul/CA/certs/${node_name}.pem"
    } ~> Service['consul']
    $cert_file = '/opt/consul/consul.pem'
    $key_file = '/opt/consul/consul.key'
  } else {
    $cert_file = undef
    $key_file = undef
    warning("You haven't signed the consul cert for ${node_name} yet.")
  }

  file { '/etc/consul/acl_master_token.json':
    ensure  => present,
    content => '{}',
    replace => false,
    backup  => false,
    mode    => '0400',
    owner   => 'consul',
  } ~> Service['consul']

  class { 'consul':
    config_hash => {
      'datacenter'          => $datacenter,
      'data_dir'            => '/opt/consul',
      'ui_dir'              => '/opt/consul/ui',
      'client_addr'         => '0.0.0.0',
      'node_name'           => $node_name,
      'server'              => $serve,
      'retry_join'          => $servers,
      'bootstrap_expect'    => $bootstrap_expect,
      'disable_remote_exec' => true,
      'ca_file'             => '/opt/consul/ca.crt',
      'verify_outgoing'     => true,
      'verify_incoming'     => ($cert_file != undef),
      'cert_file'           => $cert_file,
      'key_file'            => $key_file,
      'acl_datacenter'      => 'bkm',
      'acl_default_policy'  => 'deny',
      'advertise_addr'      => $::default_route_source_ip,
    },
  }

  if ($node_name in $monitors) {
    class { 'consul_alerts':
      consul_datacenter => $datacenter,
      consul_acl_token  => '',
    }
  }
}

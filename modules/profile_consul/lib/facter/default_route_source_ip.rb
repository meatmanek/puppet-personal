Facter.add('default_route_source_ip') do
  setcode do
    Facter::Core::Execution.exec('ip route get 8.8.8.8 | sed -E -n "s/.* src ([^ ]*).*/\1/p"')
  end
end

#!/bin/bash

set -euC

if [[ $# -lt 1 ]]; then
    echo "Usage: $0 <host> [<host> ...]"
fi

read -s -p "Master token? " TOKEN
for host in "$@"; do
    ssh "$host" -- sudo tee /etc/consul/acl_master_token.json >/dev/null <<____END
        {"acl_master_token":"${TOKEN}"}
____END
done
#!/bin/bash

set -xeuC

cd "${BASH_SOURCE%/*}/../files/CA"

hostnames=("$@")
identities=()
for hostname in "${hostnames[@]}"; do
    identity="$(ssh "$hostname" hostname --fqdn)"
    identities+=("$identity")
    scp "$hostname":/opt/consul/consul.csr csrs/"$identity".csr
    openssl ca -config openssl.cnf -out certs/"$identity".pem -infiles csrs/"$identity".csr
done

for identity in "${identities[@]}"; do
    echo "consul::cert_signed::${identity}: true" >> ../../data/common.yaml
done

class packages::sdr {
  if $::architecture == 'armv7l' and $::lsbdistcodename == 'trusty' {
    apt::source { 'proposed':
      location => 'http://ports.ubuntu.com/ubuntu-ports',
      release  => "trusty-proposed",
      repos    => 'restricted main multiverse universe',
    }
  }
  package { ['rtl-sdr', 'gnuradio', 'gr-osmosdr', 'hackrf']:
    ensure => 'latest',
  }
}
class packages::desktop_environment {
  package { ['fluxbox', 'xterm', 'chromium', 'xorg-server', 'xorg-xinit']:
    ensure => latest,
  }
}
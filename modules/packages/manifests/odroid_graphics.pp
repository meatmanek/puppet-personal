class packages::odroid_graphics {
  package { ['odroid-libgl', 'xf86-video-fbturbo-git',]:
    ensure => latest
  }
}
class packages::system_tools {
  package { [
    'git',
    'pkg-config',
    'gawk',
    'tmux',
    'irssi',
    'arping',
    'pssh',
  ]:
    ensure => 'latest',
  }
  if $::operatingsystem != 'darwin' {
    # these things are provided on mac and I don't feel like
    package { [
      'vim',
      'make',
      'gcc',
      'curl',
      'bc',
      'python-virtualenv',
      'cmake',
      'usbmount',
      'exfat-utils',
      'strace',
      'htop',
      'ruby-shadow', # so puppet can set passwords
    ]:
      ensure => 'latest',
    }
  } else {
    package { [
      'htop-osx',
    ]:
      ensure => 'latest',
    }
  }
}

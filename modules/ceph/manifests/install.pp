class ceph::install(
  $version,
) {
  apt::source { 'ceph':
    location  => 'http://download.ceph.com/debian-jewel/',
    repos     => 'main',
    key       => {
      'id'      => '08B73419AC32B4E966C1A330E84AC2C0460F3994',
      'content' => file('ceph/ceph.key'),
    }
  } ->
  package { ['ceph', 'ceph-mds']:
    ensure => $version,
  }
}

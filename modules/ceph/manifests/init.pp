class ceph(
  $version = "10.2.2-1${::lsbdistcodename}"
) {
  class { 'ceph::install':
    version => $version,
  }
}

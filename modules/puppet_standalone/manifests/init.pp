class puppet_standalone {
  if $::operatingsystem != 'Darwin' {
    service { 'puppet':
      ensure => stopped,
      enable => false,
    }
  }

  file { '/usr/local/bin/run-puppet':
    source => 'puppet:///modules/puppet_standalone/run-puppet',
    mode   => '0755',
  }
  if $::operatingsystem != 'darwin' {
    cron::hourly { 'puppet':
      minute  => fqdn_rand(60, 'puppet cron job'),
      user    => 'root',
      command => '/usr/local/bin/run-puppet',
    }
  }
}
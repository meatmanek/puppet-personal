class role::laptop {
  include profile_mac
  include profile_common
  package { 'sublime-text':
    provider => brewcask,
    ensure => present,
  }
}
class role::storage() {
  include profile_common
  include profile_duo
  include nfs::server

  include ceph
  include zerotier
}

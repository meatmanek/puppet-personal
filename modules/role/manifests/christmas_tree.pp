class role::christmas_tree {
  include profile_common

  file { '/usr/local/bin/check_water_level.sh':
    source => 'puppet:///modules/role/check_water_level.sh',
    owner  => 'root',
    group  => 'consul',
    mode   => '4754',
  }

  sudo::conf { 'consul_can_check_water_level':
    priority => 50,
    content  => 'consul ALL=(ALL) NOPASSWD: /usr/local/bin/check_water_level.sh',
  }

  # TODO: set up sudo rule for read_water_level.py, install it
  # (and the device tree overlay) somehow; maybe a package?
  consul::check { 'water_level':
    interval => '60s',
    script   => 'sudo /usr/local/bin/check_water_level.sh'
  }
}

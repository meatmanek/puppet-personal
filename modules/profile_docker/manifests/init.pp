# == class: profile_docker
#
class profile_docker(
  $base_version = '1.12.1',
) {
  if $::operatingsystem == 'Darwin' {
    package { 'boot2docker':
      ensure   => 'present',
      provider => 'brewcask',
    }
  } else {
    if $::architecture == 'armv7l' {
      apt::source { 'hypriot':
        location          => 'https://packagecloud.io/Hypriot/Schatzkiste/debian/',
        release           => 'wheezy',
        repos             => 'main',
        required_packages => 'debian-keyring debian-archive-keyring',
        key               => {
          'id'      => '418A7F2FB0E1E6E7EABF6FE8C2E73424D59097AB',
          'content' => file('profile_docker/packagecloud.key'),
        },
        pin               => '10',
        include_src       => false,
      } -> Exec['apt_update'] -> Package['docker']

      if $::operatingsystem == 'Ubuntu' and versioncmp($::operatingsystemrelease, 15.04) < 0 {
        # docker-hypriot is meant for modern debian (systemd) so I have to
        # include my own upstart config.
        file { '/etc/init/docker.conf':
          source => 'puppet:///modules/profile_docker/docker.conf',
        }
      } else {
        file { '/etc/init/docker.conf':
          ensure => absent,
        }
      }

      class { 'docker':
        use_upstream_package_source => false,
        manage_kernel               => false,
        package_name                => 'docker-hypriot',
        ensure                      => "${base_version}-1",
        docker_users                => ['meatmanek'],
      }
    } else {
      class { 'docker':
        version => "${base_version}-0~${::lsbdistcodename}",
        docker_users                => ['meatmanek'],
      }
    }
  }
}

class profile_duo() {

  class { 'duo_unix':
    usage             => 'login',
    ikey              => '{{ key "secret/duo/ikey" }}',
    skey              => '{{ key "secret/duo/skey" }}',
    host              => '{{ key "secret/duo/api_host" }}',
    pushinfo          => 'yes',
    autopush          => 'yes',
    accept_env_factor => 'yes',
  }
  # fix up ordering issues that prevent this from working on the first try
  Exec['Duo Security GPG Import'] -> Exec['duo-security-apt-update']

  class { 'consul_template':
    install_method => 'package',
  }

  file {'/opt/consul/templates/':
    ensure => 'directory',
    owner  => 'consul',
  } ->
  File <|title == '/etc/duo/login_duo.conf'|> {
    path => '/opt/consul/templates/login_duo.conf.ctmpl'
  } ->

  exec { 'actually create duo conf':
    command => shellquote(['bash', '-c', 'CONSUL_HTTP_TOKEN="$(< /opt/consul/tokens/root)" consul-template -once -template="/opt/consul/templates/login_duo.conf.ctmpl:/etc/duo/login_duo.conf:chmod 0600 /etc/duo/login_duo.conf && chown sshd /etc/duo/login_duo.conf"']),
  }
}

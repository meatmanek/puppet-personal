class profile_sudo() {
  if $::operatingsystem != 'darwin' {
    class { 'sudo': }
    sudo::conf { 'sudo_group_can_sudo':
      priority => 50,
      content  => '%sudo ALL=(ALL) ALL',
    }
  }
}

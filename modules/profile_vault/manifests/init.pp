class profile_vault(
  $servers = hiera('profile_vault::servers', undef),
) {

  if $::fqdn in $servers {
    file { '/etc/vault/consul_token.json':
      ensure => 'present',
      owner  => 'root',
      mode   => '0400',
    }
    class { 'vault':
      backend => {
        consul => {
          path => 'vault/',
        }
      },
      listener => {
        tcp => {
          address       => '0.0.0.0:8200',
          tls_cert_file => '/opt/consul/consul.pem',
          tls_key_file  => '/opt/consul/consul.key',
        }
      },
    }
  } else {
    class { 'vault': }
  }

}
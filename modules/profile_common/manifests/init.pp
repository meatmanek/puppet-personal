class profile_common() {
  include users
  include puppet_standalone
  include packages::system_tools
  include profile_apt
  include profile_consul
  include profile_vault
  include profile_mdns
  include profile_docker
  include profile_sudo
  include profile_ntp
}

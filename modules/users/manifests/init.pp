class users {
  if $operatingsystem == 'Darwin' {
    user { 'meatmanek':
      # groups     => ['admin', 'video'],
      ensure     => present,
      managehome => false,
      shell      => '/bin/bash',
    }
  } else {
    user { 'meatmanek':
      groups     => ['sudo'],
      ensure     => present,
      managehome => true,
      password   =>'$6$jopI2LaQjg$0vtKVdZ4IvWbwoMOQ/U2RSwpQ2by1751.KCN/dZsmmgIW4jGs0iGcvYELaqUPiE8I8gje7RnLmpZK17ijGORi.',
      shell      => '/bin/bash',
    }
  }
  User['meatmanek'] ->
  ssh_authorized_key { 'meatmanek@juroujin.local':
    user => 'meatmanek',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCcA1oYMMcGSrcxojdiI3Mbp5E/GeT1z+gYOqQe9dY0mxmC56oZxRPzm/RjHdcykp+oMmZTcNaXckZjTM2azw/DkskaJgA3jDJPRsz1GGA6qmGQvc9AWA20xaEq2jhiAqd1ELl62rWfe4V7cq5AyCJsh7d/uFbbzFxBWqe7oevd++F94m5VZBKZSdlluiUbZgBHd/jygS20EBMRA/7OnRpoJVxs6gNvvfDkUrZDLTjpUtlYKQ4WICeDGxipS+xD698dsgeWBx6WGFK+2DVlxARXV/L43AV2dNVTJKeIrvHGcPcNTvj7B1Gu0jBSycNFy1T9/3WL2Pu8B6eFOdCF3AnfLwUeTAQJsXjgwAxRRp1eOVviIh52mr64gY6SF+R6KTSQsYSC4Ux5IkqGSZ+Y0ef1sMQWansOmkrsCsA84n89XQC5URciNiH3aB9Uk4URTtivPGO+BQyoMgID036v4gzhrAOX4DiaiMtG0AdUc+LlU00nmBve8tIJP/SCGiY3LQUBipxZMnsZ3Wt/z3r3HUMkfftiir4T83nwLynKK0GkniNOp8LAYixZc9rqy7MXz5/yoYpejLArbB/C4DehdTcvZHvF1zgY93Jf6AWA+GUUdFw5QR/k3rYJf/lGcJG0V4rVcmWGvWr2b5aE1YV4PGksQUSypSyzifj6YIja3KCc9w==',
  } ->
  ssh_authorized_key { 'meatmanek@juroujin-ubuntu':
    user => 'meatmanek',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDD+uT7KWzlks0w2jx3K5/LmFRBwrp9Sp2W7wll1ifNFQa9AksHcLt2kwrM0Nvbh+so8McLYMsB8yJsVC40+bAEBhmakQ1qnpgisdtOodbaE1ZXdxpeWr/z5owjwGfJroI4FlmiB9Kb2Dd2qiDqWSK5v04grYaTaiYNuL1ROu8qPQqXUEpXfA0iEmzNsbjlmU7tHEILNyzNEgLh8DX+n47w/RuJ7ywzkF/pZRHB7wOhJNx1B0ky2HYNhr6rrCvXGdFDXK+3ir4YNa7ZvlnuHztN6wHtXzkb7vPifIwrQBOGPIUuoYJjf4i/tgbcUqRuuGjVLbv2Bqpff38S7pcwC7tp',
  }

  user { 'teresita':
    ensure     => present,
    managehome => $operatingsystem ? {
      'Darwin' => false,
      default  => true,
    },
    password   => undef,
    shell      => '/bin/bash',
  } ->
  ssh_authorized_key { 'teresas@public.key':
    user => 'teresita',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDeImMrlnDx0A8N1bnnliTNIsR9jdlGghGO+ph7/EIu8oHoVNcmDxDEoqUfF+xZ3Ov+dMLVcvHpICw+kODNOpemZusI44pjmH7h/tLTSkqnrGXrpjfV8WKt3VTx0h8k4WgHCgNU+sjU2qdrMUvapLYbVU+fUmOf7nP0xRpQ6MFlWkp+iKC4HiMEwoVhmNO94YCnW12M4T9hhWDdsUnJpOizQ/K02ZRQBRiCo/RCf06YQoTtuVQ66zB5z9BG/IS3qHOcaE+oCvkO6B7bTd1THRlpLVoZkeBCV76iurMdGD0TaEBqgZkRMM8zO6jMTenza3Ja+BUcrXjeb6OvBt2smJyvDdvjEc+CgqJjep0UNM3lRXbSZYCp333hcRhg138fRyUETUHQN26+jaOctj0XaIxpjfCvZHT/xfFJIHDSY2+9XGWTB2kWWdXIOsiuMu8LyvyMy4O37b+servwt54Q2cEiAqb/nbN8m1LlXQrG7ThAFaXT+xGvPo+NFCls/h4RqdkgFsW+Iid72DS82FodSQSObtj5rb+oUsYpdTEmxG1KedBF3aP1VRU4OsWZukt29H3g/EQxyioyLo0MMA93Q3Z8hwnF2Sy0e7cJZjXDomtknMBq93C4ibnvTRBzNdBXIfFZVR7rVRvHrkYpyxjOHLx2g5RXEA/SRHK6AAELI8Av+w==',
  } ->
  ssh_authorized_key { ' ttenfelder@fitbit.com':
    user => 'teresita',
    type => 'ssh-rsa',
    key  => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCifIgFti9O1oKePLy4PwLal8PGhYXCAnItma5DaXibeyMsE2zFTBJUkQUpY+FhqGIKq2DCkJwMKA8IU7/4qsR+sHbNPkYigU35tPOWeKVFWZuYHeyiKjo6HgxuzHnVqfVSfz/C3WhGwtLt0xscOwUrq+SAHUW+YTrA611gRW37u83p7pVsXDFp3XrZQ/j30yUqR1Atc384R/H+WzK0/96lhebE0A9zXu+aIjPo1ZIaDLMwPjuvsrHAjcTTKp2zY699KZYdpWCiUF2RCY47HGbiUr6D//xlUan6+Hhr+kysBB2K0Ib3OnQ+Vu+Gn0igD1haQdUxepftEK/llSAQUst+Gp2O5iXq4pqB2lXXdQENweHhykBUZPiW5DMzZFbFgFndI3JmgokKNdAa9+ba7i1DrcaemyzeL2piOfwTy4zsZKrNPaxvJ0jqf2uS589gf+tYt3H4xNIfSUtJJFni5tDp3EWcqnYJiPTkCZYR2yQfuxbCGse5Z4B9JHHtw+471swfadOV0oKxRGrCzF5FkksdSuK8LSP8rPRecvbBvpIhc9GllgQxejSVWjm3MYhRvXpeu2xBvoe4J7fjfxcgzSSyemn049a/4cAFE/MpESxgxPvQXRZva1VOr/8BWq924pTqo1KHIAMlVwKrMGe91xYzyvYnBn81E8lDdhUZqtyYCQ==',
  }

  if $operatingsystem != 'Darwin' {
    user { 'clarecat':
      ensure     => present,
      managehome => $operatingsystem ? {
        'Darwin' => false,
        default  => true,
      },
      password   => undef,
      shell      => '/bin/bash',
    } ->
    ssh_authorized_key { 'clare@localhost':
      user => 'clarecat',
      type => 'ssh-rsa',
      key  => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCu+Q0kTGVeS9bN+9lE37GM0b41f+6AxpJiuB1Kc5jkHeN/1I8EJRMWfu9sJXBU9A0MQw3r0oXEEjLGz+8Rx0no/VW9qaKX4a1YkBONfIcaJ4CTaHMzGo0bF/WIbPAkM/LIIXU5p/Vsp8zDg3kU91J717VuELTSPNbbacqh51Mt8goZRTjrYSgmlJmZo8op7crILIzOvX7pzFhCKgMetqR0S5dFcSVQffZ/5Olc0/Tp+yx9vynqz9WrNDSu+6hQdCbn5Ej6y5Q4o0muWk+ArciNEiz3hpAutX8fbIfUzRYtxjNDzdW0zc+vWCw3wYR1h0LUwn9vDx4XXbKwZ2lVXlxATpHCB/VDgOeIs27PrpH3SaKOySUz1QwuE3YhZ81Phw5egZdlR7i9tAWyrX7WuuFtKV6sH0sFQ7LpWeeplRAT7Ezp87tmddDxy9ZN9e22dQ+iWhgP49lSExtx6763Xk5BAIn0NPbzMp2ZrXmshsY6E80+P81hBub3/br9zo544me1HVjmdBYaMLxx879DRlcjWfOLddoBLmMvq532VfnK3ASgvAywqdp6lbCFoVeeDanuhIaRpqg4pe6CMRmVYXq5ZK+tMcKkqhACohUl+QZDw0jRoKLwGVgghJBZHt2qM27QEjUUSPecezX/Jbb0rudTurIMjXpAWCPxl5zU6AIWZQ==',
    }
  }

}

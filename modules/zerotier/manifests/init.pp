class zerotier () {
  apt::source { 'zerotier':
    location => "http://download.zerotier.com/debian/${::lsbdistcodename}",
    key      => {
      id      => '74A5E9C458E1A431F1DA57A71657198823E52A61',
      content => file('zerotier/zerotier.key'),
    },
  }
  package { 'zerotier-one':
    ensure => 'latest',
  }->
  service { 'zerotier-one':
    ensure => 'running',
    enable => true,
  }
}

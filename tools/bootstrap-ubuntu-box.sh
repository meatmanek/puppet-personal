#!/bin/bash

set -eu

BASEDIR="${BASH_SOURCE[0]%/*}"

apt-get update
apt-get install -yq bundler python git
cd "$BASEDIR"
bundle install
./puppet-standalone
